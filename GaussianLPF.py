import cv2
import numpy as np

'''https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html'''

image = cv2.imread('cycle.jpg')
img = cv2.resize(image, (1024,720))
blur = cv2.GaussianBlur(img,(5,5),0)

cv2.imshow("Main Image", img)
cv2.imshow("After Gaussian LPF", blur)

cv2.waitKey(0)
cv2.destroyAllWindows()