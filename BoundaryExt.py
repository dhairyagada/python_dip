import cv2
import numpy as np

'''https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html'''

image = cv2.imread("cycle.jpg")
imS = cv2.resize(image, (1024,720))
gray_image = cv2.cvtColor(imS, cv2.COLOR_BGR2GRAY)

kernel = np.ones((3,3),np.uint8)

erosion = cv2.erode(gray_image,kernel,iterations = 1)
dilation = cv2.dilate(gray_image,kernel,iterations = 1)

borderimage = gray_image - erosion

cv2.imshow("Main Image", imS)
cv2.imshow("Erosion", erosion)
cv2.imshow("Dilation", dilation)
cv2.imshow("Boundary", borderimage)

cv2.waitKey(0)
cv2.destroyAllWindows()