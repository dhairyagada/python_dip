## Some Links

[Boundary Extraction](https://docs.opencv.org/trunk/d9/d61/tutorial_py_morphological_ops.html)

[Filtering](http://imgsimon.blogspot.com/2016/05/python-image-filtering-in-python-and.html)

[Gaussian LPF](https://docs.opencv.org/3.1.0/d4/d13/tutorial_py_filtering.html)